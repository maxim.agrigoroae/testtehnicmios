import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ErrorResponse } from '../../../commons/model/error-response';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;
  error: ErrorResponse;
  loading: boolean;

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.loading = true;
    this.authService.login({ email: this.email, password: this.password })
      .then(() => {
        this.error = null;
        this.loading = false;
        this.router.navigate(['/']);
      })
      .catch(error => {
        this.error = error;
        this.loading = false;
      });
  }
}
