import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private tokenKey = 'clientapp-angular-auth-token';

  constructor(
    private httpClient: HttpClient
  ) {}

  protected storeAuthToken(token: string) {
    localStorage.setItem(this.tokenKey, token);
  }

  protected removeToken() {
    localStorage.removeItem(this.tokenKey);
  }

  getToken(): string {
    return localStorage.getItem(this.tokenKey);
  }

  hasToken(): boolean {
    return this.getToken() !== null;
  }

  login({ email, password }): Promise<any> {
    return this.httpClient.post(`${environment.api.baseUrl}/api/auth/login`, { email, password })
      .toPromise()
      .then((response: any) =>
        this.storeAuthToken(response.token)
      );
  }

  logout() {
    this.removeToken()

    // const headers = new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   'Authorization': 'Bearer ' + this.getToken()
    // });

    // return this.httpClient.post(`${environment.api.baseUrl}/api/auth/logout`, {}, { headers })
    //   .toPromise()
    //   .then(() =>
    //     this.removeToken()
    //   );
  }
}
