export class ErrorResponse {
  code: string;
  msg: string;

  constructor(code: string, msg: string) {
    this.code = code;
    this.msg = msg;
  }
}
