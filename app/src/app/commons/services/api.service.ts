import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    protected http: HttpClient,
    protected router: Router,
    protected authService: AuthService
  ) { }

  protected get baseUrl(): string {
    return environment.api.baseUrl;
  }

  private get headers(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken()
    });
  }

  protected serialize(obj: any, prefix: any): string {
    const str = [];

    for (const p in obj) {
      if (obj.hasOwnProperty(p)) {
        const k = prefix ? prefix + '[' + p + ']' : p;
        const v = obj[p];

        str.push((v !== null && typeof v === 'object') ?
          this.serialize(v, k) :
          encodeURIComponent(k) + '=' + encodeURIComponent(v));
      }
    }

    return str.join('&');
  }

  protected sendPost(url: string, data): Promise<any> {
    return this.http.post(url, data, { headers: this.headers })
      .toPromise()
      .catch(response => this.handleError(response));
  }

  protected sendPut(url: string, data): Promise<any> {
    return this.http.put(url, data, { headers: this.headers })
      .toPromise()
      .catch(response => this.handleError(response));
  }

  protected sendGet(url: string): Promise<any> {
    return this.http.get(url, { headers: this.headers })
      .toPromise()
      .catch(response => this.handleError(response));
  }

  protected sendDelete(url: string): Promise<any> {
    return this.http.delete(url, { headers: this.headers })
      .toPromise()
      .catch(response => this.handleError(response));
  }

  protected handleError(response: any): Promise<any> {
    console.error('An error occurred', response);

    const { error } = response;

    if (response.status === 401) {
      this.router.navigate(['/login']);
    }

    return Promise.reject(error);
  }
}
