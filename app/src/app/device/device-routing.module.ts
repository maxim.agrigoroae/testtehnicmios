import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/guards/auth.guard';
import { DeviceListComponent } from './components/device-list/device-list.component';

const routes: Routes = [
  { path: 'devices', component: DeviceListComponent, canActivate: [AuthGuard] },
  // { path: 'users/:id', component: UserPageComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeviceRoutingModule { }
