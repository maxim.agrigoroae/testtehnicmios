import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeviceComponent } from './components/device/device.component';
import { DeviceListComponent } from './components/device-list/device-list.component';
import { DevicePageComponent } from './components/device-page/device-page.component';
import { DeviceRoutingModule } from './device-routing.module';

@NgModule({
  declarations: [
    DeviceComponent,
    DevicePageComponent,
    DeviceListComponent,
  ],
  imports: [
    CommonModule,
    DeviceRoutingModule
  ]
})
export class DeviceModule { }
