import { Injectable } from '@angular/core';
import { ApiService } from '../../commons/services/api.service';
import { Device } from '../device.model';

@Injectable({
  providedIn: 'root'
})
export class DeviceService extends ApiService {

  private endpoint = `${this.baseUrl}/api/devices`;

  protected handleVariableKeys(devices) {
    return devices.map((d) =>  {
      const variablesKeys = Object.keys(d.variables);
      const variables = variablesKeys.map(key => ({ key, value: d.variables[key] }))
      return {
        ID: d.ID,
        name: d.name,
        variables
      }
    })
  }

  getAll(): Promise<any> {
    const url = this.endpoint;

    return this.sendGet(url)
      .then((data: any) =>({
        recordsTotal: data.length,
        recordsFiltered: data.length,
        data: this.handleVariableKeys(data.map(d => new Device(d)))
      })
    );
  }

  get(ID: string): Promise<any> {
    return this.sendGet(`${this.endpoint}/${ID}`)
      .then(data => this.handleVariableKeys(new Device(data)));
  }

  create(device: Device): Promise<any> {
    return this.sendPost(`${this.endpoint}`, device);
  }

  update(ID: string, data): Promise<any> {
    return this.sendPut(`${this.endpoint}/${ID}`, data);
  }
}
