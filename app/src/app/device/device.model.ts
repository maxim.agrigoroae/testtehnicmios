export class Device {

  ID: string;
  name: string;
  variables: object;

  constructor(data: any) {
    this.ID = data.ID;
    this.name = data.name;
    this.variables = data.variables;
  }
}
