import { Component, OnInit, Input } from '@angular/core';
import { DeviceService } from '../../services/device.service';
import { Device } from '../../device.model';

@Component({
  selector: 'app-device-page',
  templateUrl: './device-page.component.html',
})
export class DevicePageComponent implements OnInit{

  @Input() device: Device[];

  constructor(
    private deviceService: DeviceService
  ) {

  }

  ngOnInit() {}
}
