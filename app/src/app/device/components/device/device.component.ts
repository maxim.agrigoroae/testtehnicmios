import { Component, OnInit, Input } from '@angular/core';
import { DeviceService } from '../../services/device.service';
import { Device } from '../../device.model';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
})
export class DeviceComponent implements OnInit{

  @Input() device: Device[];

  constructor(
    private deviceService: DeviceService
  ) {

  }

  ngOnInit() {}
}
