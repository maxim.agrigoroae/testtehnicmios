import { Component, OnInit } from '@angular/core';
import * as io from 'socket.io-client';
import { DeviceService } from '../../services/device.service';
import { Device } from '../../device.model';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
})
export class DeviceListComponent implements OnInit{

  devices: Device[];
  socket: SocketIOClient.Socket;

  constructor(
    private deviceService: DeviceService
  ) {
    this.socket = io.connect('http://127.0.0.1:9000');
  }

  ngOnInit() {
    this.deviceService.getAll()
      .then((response: any) => {
        this.devices = response.data;
      });

    // this.socket.on('device-updated', (devices: Device[]) => {
    //   alert('Devices was updated!');
    //   this.devices = devices;
    //   // console.log('')
    // });

    this.socket.on('device-updated', (devices) => {
      console.log(devices);
      // this.randomizeNames();
    });
  }

  emitSocket() {
    this.socket.emit('angular-scare', {});
  }

  makeid(): string {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  randomizeNames() {
    this.devices.map((device: Device) => {

      device.name = this.makeid();

      return device;
    });
  }

}
