### API

Test tehnic MIOS api

### requirements

1. node.js ```v10.15.1``` or higher
2. mongodb

### local dev requirements

1. node.js ```v10.15.1``` or higher
2. docker ```^Docker version 18.03.0-ce``` / docker-compose ```^1.20.1```

### local environment deploy

1. clone the repository
2. run ```npm install``` from repository
3. run ```docker-compose up``` from repository, waiting when containers is done.
4. configure environment local variables (if need, watch section below how to do it)
5. run nodejs server ```npm start```, also you can run ```npm run debug``` to run app in special mode to debug via attaching.
6. open ```localhost:{YOUR_PORT}/docs``` to meet API available endpoint.
7. Enjoy!

### api environment variables

<table>
  <tr>
    <th>Variable</th>
    <th>Default Value</th>
    <th>Description</th>
    <th>Available Values</th>
    <th>Is required</th>
  </tr>
  <tr>
    <td><tt>NODE_ENV</tt></td>
    <td>'local'</td>
    <td>Node js environment mode</td>
    <td>[ 'local', 'development', 'production', 'qa', 'staging' ]</td>
    <td><tt>true</tt></td>
  </tr>
  <tr>
    <td><tt>PORT</tt></td>
    <td>'9000'</td>
    <td>Application server port</td>
    <td>any number</td>
    <td><tt>false</tt></td>
  </tr>

  <tr>
    <td><tt>IP</tt></td>
    <td>'0.0.0.0'</td>
    <td>Application server IPv4 address</td>
    <td><tt>-</tt></td>
    <td><tt>false</tt></td>
  </tr>
  <tr>
    <td><tt>JWT_SECRET</tt></td>
    <td>'default-jwt-secret'</td>
    <td>jsonwebtoken secret encrypt key</td>
    <td><tt>-</tt></td>
    <td><tt>false</tt></td>
  </tr>
  <tr>
    <td><tt>JWT_EXPIRATION</tt></td>
    <td>'24h'</td>
    <td>jsonwebtoken experation after encode</td>
    <td><tt>-</tt></td>
    <td><tt>false</tt></td>
  </tr>
  <tr>
    <td><tt>MONGOOSE_HOST</tt></td>
    <td><tt>-</tt></td>
    <td>MongoDB or Mongo Cluster uri hostname (without dbName, auth etc.)</td>
    <td><tt>-</tt></td>
    <td><tt>true</tt></td>
  </tr>
  <tr>
    <td><tt>MONGOOSE_AUTH_USER</tt></td>
    <td><tt>-</tt></td>
    <td>MongoDB username</td>
    <td><tt>-</tt></td>
    <td><tt>true</tt></td>
  </tr>
  <tr>
    <td><tt>MONGOOSE_AUTH_PASS</tt></td>
    <td><tt>-</tt></td>
    <td>MongoDB password</td>
    <td><tt>-</tt></td>
    <td><tt>true</tt></td>
  </tr>
  <tr>
    <td><tt>MONGOOSE_DBNAME</tt></td>
    <td><tt>-</tt></td>
    <td>MongoDB database name</td>
    <td><tt>-</tt></td>
    <td><tt>true</tt></td>
  </tr>
  <tr>
    <td><tt>MONGOOSE_AUTO_INDEX</tt></td>
    <td><tt>-</tt></td>
    <td>MongoDB auto index</td>
    <td><tt>false</tt></td>
    <td><tt>true</tt></td>
  </tr>
  <tr>
    <td><tt>HOSTNAME</tt></td>
    <td><tt>-</tt></td>
    <td>Application Hostname</td>
    <td><tt>-</tt></td>
    <td><tt>true</tt></td>
  </tr>
</table>

### example of env variables

<table>
  <tr>
    <th>Variable</th>
    <th>Value</th>
  </tr>
  <tr>
    <td><tt>NODE_ENV</tt></td>
    <td>production</td>
  </tr>
  <tr>
    <td><tt>HOSTNAME</tt></td>
    <td>{DEPLOYED_HOSTNAME}</td>
  </tr>
  <tr>
    <td><tt>MONGOOSE_DBNAME</tt></td>
    <td>testtehnicmios</td>
  </tr>
  <tr>
    <td><tt>MONGOOSE_HOST</tt></td>
    <td>{MONGO_URI}</td>
  </tr>
  <tr>
    <td><tt>MONGOOSE_AUTH_USER</tt></td>
    <td>testtehnicmios</td>
  </tr>
  <tr>
    <td><tt>MONGOOSE_AUTH_PASS</tt></td>
    <td>test</td>
  </tr>
  <tr>
    <td><tt>MONGOOSE_AUTO_INDEX</tt></td>
    <td>false</td>
  </tr>
</table>
