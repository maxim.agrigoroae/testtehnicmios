import mongoose from 'mongoose';
import timestamps from 'mongoose-timestamp';

import mongooseStringQuery from 'mongoose-string-query';

const MigrationSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  }
});

MigrationSchema.plugin(timestamps);
MigrationSchema.plugin(mongooseStringQuery);

export default mongoose.model('Migration', MigrationSchema);
