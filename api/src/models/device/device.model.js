import mongoose from 'mongoose';
import timestamps from 'mongoose-timestamp';
import mongooseStringQuery from 'mongoose-string-query';

import schema from '../../utilities/database/schema';

const { HasActive } = schema;

const DeviceSchema = new mongoose.Schema(Object.assign({}, HasActive, {
  ID: {
    type: 'string',
    required: true,
    unique: true
  },
  name: {
    type: 'string',
    required: true,
    unique: true
  },
  variables: {
    type: 'mixed',
    default: {}
  }
}));

DeviceSchema.plugin(timestamps);
DeviceSchema.plugin(mongooseStringQuery);

export default mongoose.model('Device', DeviceSchema);
