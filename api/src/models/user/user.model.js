import mongoose from 'mongoose';
import bcrypt from 'mongoose-bcrypt';
import timestamps from 'mongoose-timestamp';
import mongooseStringQuery from 'mongoose-string-query';

import schema from '../../utilities/database/schema';

const { HasActive } = schema;

const UserSchema = new mongoose.Schema(Object.assign({}, HasActive, {
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true,
    bcrypt: true
  }
}));

UserSchema.plugin(bcrypt);
UserSchema.plugin(timestamps);
UserSchema.plugin(mongooseStringQuery);

export default mongoose.model('User', UserSchema);
