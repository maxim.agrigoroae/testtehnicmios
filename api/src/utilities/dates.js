const addDays = (date, days) => {
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
};

const addMinutes = (date, minutes) => {
  const result = new Date(date);
  result.setMinutes(result.getMinutes() + minutes);
  return result;
};

const compareDates = (d1, d2) => {
  if (d1.getTime() === d2.getTime()) {
    return 0;
  } else if (d1.getTime() > d2.getTime()) {
    return 1;
  } else if (d1.getTime() < d2.getTime()) {
    return -1;
  }

  return -99;
};

export default {
  addDays, compareDates, addMinutes
};
