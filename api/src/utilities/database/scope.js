export default {
  whereActive(active = true) {
    return Object.assign({}, { active });
  }
};
