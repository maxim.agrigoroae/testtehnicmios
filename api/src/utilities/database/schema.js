const HasActive = {
  active: {
    type: Boolean,
    required: true,
    default: true
  }
};

export default {
  HasActive
};
