/* eslint import/prefer-default-export:0 */

import httpStatus from 'http-status';

/**
 * Class representing an server-side response.
 */
export class APIResponse {
  /**
   * Creates an APIResponse.
   * @param {string} message - Response message.
   * @param {number} code - HTTP status code.
   * @param {string} type - Response type.
   * @param {any} data - Response type.
   */
  constructor(message, code, type, data = {}) {
    this.message = message;
    this.code = code;
    this.type = type;
    this.data = JSON.stringify(data);
  }
}

/**
 * Class representing an server-side response.
 * @extends APIResponse
 */
export class InternalErrorResponse extends APIResponse {
  /**
   * Creates an InternalErrorResponse.
   * @param {string} message - Response message.
   * @param {number} code - HTTP status code.
   * @param {string} type - Response type.
   * @param {any} data - Response type.
   */
  constructor(message, code = httpStatus.INTERNAL_SERVER_ERROR, type = 'error', data) {
    super(message, code, type, data);
  }
}

/**
 * Class representing an server-side health status response.
 * @extends APIResponse
 */
export class HealthResponse extends APIResponse {
  /**
   * Creates an InternalErrorResponse.
   * @param {string} message - Response message.
   * @param {number} code - HTTP status code.
   * @param {string} type - Response type.
   */
  constructor(message = 'OK', code = httpStatus.OK, type = 'success') {
    super(code, type, message);
  }
}
