/* eslint import/prefer-default-export:0 */

import httpStatus from 'http-status';

/**
 * @extends Error
 */
export class ExtendableError extends Error {
  constructor(message, status, isPublic) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
    this.status = status;
    this.isPublic = isPublic;
    this.isOperational = true; // This is required since bluebird 4 doesn't append it anymore.
    Error.captureStackTrace(this, this.constructor.name);
  }
}

/**
 * Class representing an client-side error.
 * @extends ExtendableError
 */
export class BadRequestError extends ExtendableError {
  /**
   * Creates an API error.
   * @param {string} message - Error message.
   * @param {number} status - HTTP status code of error.
   * @param {boolean} isPublic - Whether the message should be visible to user or not.
   */
  constructor(message = 'Bad Request', status = httpStatus.BAD_REQUEST, isPublic = true) {
    super(message, status, isPublic);
  }
}

/**
 * Class representing an client-side error.
 * @extends ExtendableError
 */
export class ForbiddenError extends ExtendableError {
  /**
   * Creates an API error.
   * @param {string} message - Error message.
   * @param {number} status - HTTP status code of error.
   * @param {boolean} isPublic - Whether the message should be visible to user or not.
   */
  constructor(message = 'Forbidden', status = httpStatus.FORBIDDEN, isPublic = true) {
    super(message, status, isPublic);
  }
}

/**
 * Class representing an client-side error.
 * @extends ExtendableError
 */
export class NotFoundError extends ExtendableError {
  /**
   * Creates an API error.
   * @param {string} message - Error message.
   * @param {number} status - HTTP status code of error.
   * @param {boolean} isPublic - Whether the message should be visible to user or not.
   */
  constructor(message = 'EntityNotFound', status = httpStatus.NOT_FOUND, isPublic = true) {
    super(message, status, isPublic);
  }
}

/**
 * Class representing an client-side error.
 * @extends ExtendableError
 */
export class UnauthorizedError extends ExtendableError {
  /**
   * Creates an API error.
   * @param {string} message - Error message.
   * @param {number} status - HTTP status code of error.
   * @param {boolean} isPublic - Whether the message should be visible to user or not.
   */
  constructor(message = 'Unauthorized', status = httpStatus.UNAUTHORIZED, isPublic = true) {
    super(message, status, isPublic);
  }
}

/**
 * Class representing an API error.
 * @extends ExtendableError
 */
export class APIError extends ExtendableError {
  /**
   * Creates an API error.
   * @param {string} message - Error message.
   * @param {number} status - HTTP status code of error.
   * @param {boolean} isPublic - Whether the message should be visible to user or not.
   */
  constructor(message, status = httpStatus.INTERNAL_SERVER_ERROR, isPublic = false) {
    super(message, status, isPublic);
  }
}
