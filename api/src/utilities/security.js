import httpStatus from 'http-status';
import { intersection } from 'lodash';
import passport from 'passport';

export const authenticate = (cb = undefined) => passport.authenticate('jwt', {
  session: false,
}, cb);
