import jwt from 'jsonwebtoken';
import { merge } from 'lodash';

import config from '../config/environment';

export const encode = (payload, options = {}) => jwt.sign(
  payload,
  config.JWT_SECRET,
  merge({
    expiresIn: config.JWT_EXPIRATION
  }, options)
);

export const decode = token => jwt.decode(token, config.JWT_SECRET);

export const verify = token => jwt.verify(token, config.JWT_SECRET);
