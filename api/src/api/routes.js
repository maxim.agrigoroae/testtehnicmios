import AuthModule from './auth';
import UserModule from './user';
import DeviceModule from './device';

import { authenticate } from '../utilities/security';

import { HealthResponse } from '../utilities/resources/responses';

const bodyParser = require('body-parser');

export default (app) => {
  app.use(bodyParser.urlencoded({
    extended: true
  }));

  app.use(bodyParser.json());

  app.use(
    '/api/*',
    (req, res, next) => {
      res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
      res.header('Expires', '-1');
      res.header('Pragma', 'no-cache');

      next();
    }
  );

  // Public or mixed endpoints
  app.get('/api/health', (req, res) => res.json(new HealthResponse()).send());
  app.get('/', (req, res) => res.status(200).send());

  // Auth endpoints
  app.use('/api/auth', AuthModule);

  // Private endpoints
  app.use('/api/users', authenticate(), UserModule);
  app.use('/api/devices', authenticate(), DeviceModule);
};
