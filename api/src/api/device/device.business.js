import mongoose from '../../config/mongoose';
import config from '../../config/environment';
import scopes from '../../utilities/database/scope';
import { NotFoundError, BadRequestError } from '../../utilities/resources/errors';

import socket from '../../config/socket';

const {
  Device,
} = mongoose;

const {
  whereActive
} = scopes;

export default {
  async getByID(ID) {
    const scope = Object.assign({}, whereActive(), { ID });

    const device = await Device.findOne(scope);

    if (!device) {
      throw new NotFoundError();
    }

    return device;
  },

  async list() {
    const scope = Object.assign({}, whereActive());
    const devices = await Device.find(scope);

    return devices;
  },

  async create(device) {
    const created = await (new Device(device)).save();

    socket.io.emit('device-updated', {});

    return created;
  },

  async update(ID, device) {
    const updated = await Device.updateOne({ ID }, device);

    socket.io.emit('device-updated', {});

    return updated;
  },

  async destroy(ID) {
    const deleted = await Device.deleteOne({ ID });

    return deleted;
  },

  async setVariable(ID, variable, variableValue) {
    const device = await this.getByID(ID);

    if (!config.device.overwriteVars) {
      if (Object.prototype.hasOwnProperty.call(device.variables, variable)) {
        throw new BadRequestError('Variable with that key already exists');
      }
    }

    device.variables[variable] = variableValue;
    device.markModified('variables');

    const saved = await device.save();

    return saved;
  },

  async getVariable(ID, variable) {
    const device = await this.getByID(ID);

    if (!Object.prototype.hasOwnProperty.call(device.variables, variable)) {
      throw new NotFoundError('Variable with that key does not exists');
    }

    const value = device.variables[variable];

    if (!value) {
      return null;
    }

    return value;
  },

  async removeVariable(ID, variable) {
    const device = await this.getByID(ID);

    delete device.variables[variable];

    device.markModified('variables');

    const removed = await device.save();

    return removed;
  },

  async getVariables(ID) {
    const device = await this.getByID(ID);

    return device.variables;
  }

};
