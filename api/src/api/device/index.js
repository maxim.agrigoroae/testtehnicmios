import ApiRouter from '../../utilities/resources';

import DeviceBusinessService from './device.business';

const router = new ApiRouter();

export default router
  .get('/', req => DeviceBusinessService.list(req.query))
  .get(
    '/:ID/variable',
    req => DeviceBusinessService.getVariable(req.params.ID, req.query.variable),
  )
  .get(
    '/:ID/variables',
    req => DeviceBusinessService.getVariables(req.params.ID),
  )
  .get('/:ID', req => DeviceBusinessService.getByID(req.params.ID))
  .post('/', req => DeviceBusinessService.create(req.body))
  .put(
    '/:ID/variable',
    req => DeviceBusinessService.setVariable(req.params.ID, req.body.variable, req.body.value),
  )
  .delete(
    '/:ID/:variable',
    req => DeviceBusinessService.removeVariable(req.params.ID, req.params.variable),
  )
  .put(
    '/:ID',
    req => DeviceBusinessService.update(req.params.ID, req.body),
  )
  .delete(
    '/:ID',
    req => DeviceBusinessService.destroy(req.params.ID),
  )
  .router();
