import { encode } from '../../utilities/jwt';
// import Log from '../../utilities/log';
import { BadRequestError, NotFoundError } from '../../utilities/resources/errors';

import mongoose from '../../config/mongoose';

const {
  User,
} = mongoose;

export default {
  async login(criteria) {
    const { email, password } = criteria;

    const user = await User.findOne({ email });

    if (!user) {
      throw new NotFoundError('User does not exists');
    }

    const checkPassword = await user.verifyPassword(password);

    if (!checkPassword) {
      throw new BadRequestError('Invalid credentials');
    }

    return {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      token: encode({ id: user.id })
    };
  },

};
