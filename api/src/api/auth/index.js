import ApiRouter from '../../utilities/resources';

import AuthService from './auth.business';

const router = new ApiRouter();

export default router
  .post('/login', req => AuthService.login(req.body))
  .router();
