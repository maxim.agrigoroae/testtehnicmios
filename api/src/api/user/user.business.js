import mongoose from '../../config/mongoose';
import scopes from '../../utilities/database/scope';

const {
  User,
} = mongoose;

const {
  whereActive
} = scopes;

export default {
  async list() {
    const scope = Object.assign({}, whereActive());
    const users = await User.find(scope);

    return users.map(({ _id, email, role, createdAt }) =>
      ({ _id, email, role, createdAt }));
  },

};
