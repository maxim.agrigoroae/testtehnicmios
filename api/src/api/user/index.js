import ApiRouter from '../../utilities/resources';

import UserBusinessService from './user.business';

const router = new ApiRouter();

export default router
  .get('/', req => UserBusinessService.list(req.query))
  .router();
