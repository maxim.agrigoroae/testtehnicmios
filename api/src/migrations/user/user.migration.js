/**
 * Migrate and seed basic data of users to database.
 */

import { APIError } from '../../utilities/resources/errors';
import data from './user.seed.json';

export default async (mongoose, logger) => {
  const { User } = mongoose;

  data.forEach(async (row) => {
    try {
      const newRow = new User(row);

      await newRow.save();

      logger.info(`migrate :: User.${JSON.stringify(row)} :: seed`);
    } catch (e) {
      throw new APIError(e.message);
    }
  });
};
