import _ from 'lodash';
import recursiveReadSync from 'recursive-readdir-sync';

import mongoose from '../config/mongoose';
import Log from '../utilities/log';

const { connect } = mongoose;

const { Migration } = mongoose;

const migrations = recursiveReadSync(__dirname)
  .filter(file => _.includes(file, '.migration.js'))
  .map(file => ({
    name: file.substr(file.lastIndexOf('\\') + 1),
    script: file,
  }));

connect()
  // .catch(err => Log.error(`${err.name}: ${err.message}`))
  .then(async () => {
    let migrated = false;

    await Promise.all(await migrations.map(async (migration) => {
      const { script, name } = migration;
      const m = await Migration.findOne({ name });
      if (!m) {
        const migrationScript = require(script).default; // eslint-disable-line

        await migrationScript(mongoose, Log);

        await (new Migration({ name })).save();

        migrated = true;
      }
    }));

    return migrated;
  })
  .then((migrated) => {
    Log.info(migrated ? 'Migrated successefully!' : 'Nothing to migrate!');
  })
  .catch(err => Log.error(`${err.name}: ${err.message}`));
