/**
 * Migrate and seed basic data of devices to database.
 */

import { APIError } from '../../utilities/resources/errors';
import data from './device.seed.json';

export default async (mongoose, logger) => {
  const { Device } = mongoose;

  data.forEach(async (row) => {
    try {
      const newRow = new Device(row);

      await newRow.save();

      logger.info(`migrate :: User.${JSON.stringify(row)} :: seed`);
    } catch (e) {
      throw new APIError(e.message);
    }
  });
};
