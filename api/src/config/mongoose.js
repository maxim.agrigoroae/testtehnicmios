import Mongoose from 'mongoose';
import Bluebird from 'bluebird';

import config from './environment';
import Models from '../models';
import Log from '../utilities/log';
import { APIError } from '../utilities/resources/errors';

// make bluebird default Promise
Promise = Bluebird; // eslint-disable-line no-global-assign

const {
  host,
  options
} = config.mongoose;

const mongooseDebug = config.mongoose.debug;

const logInfoEvent = (eventName, msg) => (name) => {
  Log.info(`${eventName} :: ${msg} :: ${name}`);
};

// const logErrorEvent = eventName => (name) => {
//   Log.error(`${eventName} :: ${name}`);
// };

const dbModels = {};

Models.forEach((file) => {
  Log.info(`Loading model :: ${file}`);
  const model = require(file).default; // eslint-disable-line
  dbModels[model.modelName] = model;
});

const connect = () => Mongoose.connect(host, options);

// // print mongoose logs in dev env
if (mongooseDebug) {
  Mongoose.set('debug', (collectionName, method, query, doc) => {
    Log.info(`${collectionName}.${method} :: ${JSON.stringify(query)} :: ${JSON.stringify(doc)}`);
  });
}

Mongoose.connection.on('connected', logInfoEvent('mongoose-connection', 'connected'));
Mongoose.connection.on('reconnect', logInfoEvent('mongoose-connection', 'reconnected'));
Mongoose.connection.on('disconnected', logInfoEvent('mongoose-connection', 'disconnected'));
Mongoose.connection.on('error', (e) => {
  throw new APIError(`Mongoose, ${e.message}`);
});

// module.exports = { Mongoose, connect, ...dbModels };
export default { Mongoose, connect, ...dbModels };
