export default {
  swagger: {
    host: 'localhost:9000',
  },

  mongoose: {
    host: process.env.MONGOOSE_HOST || 'mongodb://127.0.0.1:27017',
    debug: true,
    options: {
      dbName: process.env.MONGOOSE_DBNAME || 'testtehnicmios',
      auth: {
        user: process.env.MONGOOSE_AUTH_USER || 'root',
        password: process.env.MONGOOSE_AUTH_PASS || 'example',
      },
      autoIndex: process.env.MONGOOSE_AUTO_INDEX || true,
      server: {
        socketOptions: {
          keepAlive: 1
        }
      },
    }
  },

  log: 'debug',
};
