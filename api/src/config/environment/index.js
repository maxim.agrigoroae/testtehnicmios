/* eslint global-require: 0, import/no-dynamic-require:0 */

import path from 'path';
import _ from 'lodash';

const root = path.normalize(`${__dirname}/../..`);

const all = {
  env: process.env.NODE_ENV || 'local',

  port: process.env.PORT || 9000,
  ip: process.env.IP || '0.0.0.0',

  root,
  TEMP_PATH: path.join(root, '../tmp'),

  log: 'debug',

  JWT_SECRET: process.env.JWT_SECRET || 'default-jwt-secret',
  JWT_EXPIRATION: process.env.JWT_EXPIRATION || '24h',

  mongoose: {
    host: process.env.MONGOOSE_HOST,
    debug: true,
    options: {
      dbName: process.env.MONGOOSE_DBNAME || 'testtehnicmios',
      autoIndex: process.env.MONGOOSE_AUTO_INDEX || false,
      server: {
        socketOptions: {
          keepAlive: 1
        }
      },
      auth: {
        user: process.env.MONGOOSE_AUTH_USER,
        password: process.env.MONGOOSE_AUTH_PASS,
      },
    }
  },

  swagger: {
    host: `${process.env.HOSTNAME}`
  },

  device: {
    overwriteVars: process.env.DEVICE_OVERWRITE_VARS || true,
  }
};

export default _.merge(
  all,
  require(`./${all.env}.js`).default || {},
);
