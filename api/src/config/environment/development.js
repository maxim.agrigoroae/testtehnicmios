export default {
  log: 'debug',

  mongoose: {
    host: process.env.MONGOOSE_HOST || 'mongodb://127.0.0.1:27017',
    debug: true,
    options: {
      dbName: process.env.MONGOOSE_DBNAME,
      autoIndex: process.env.MONGOOSE_AUTO_INDEX || true,
      server: {
        socketOptions: {
          keepAlive: 1
        }
      },
      auth: {
        user: process.env.MONGOOSE_AUTH_USER,
        password: process.env.MONGOOSE_AUTH_PASS,
      },
    }
  },
};
