/**
 * Socket configuration
 */
import socketIO from 'socket.io';

// import config from './environment';
import Log from '../utilities/log';

const logInfoEvent = (scope, msg) => {
  Log.info(`${scope} :: ${msg}`);
};

var io;

const configureSocket = (server) => {
  io = socketIO(server);

  io.on('connection', (socket) => {
    logInfoEvent('socket', `${socket.id} connected`);

    socket.on('disconnect', () => {
      logInfoEvent('socket', `${socket.id} disconected`)
    });
  });

  return io;
};

export default {
  configureSocket,
  io
};
