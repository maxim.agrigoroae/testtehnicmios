/**
 * Passport configuration
 */

import { merge } from 'lodash';
import passport from 'passport';
import passportJWT from 'passport-jwt';

import config from './environment';
import mongoose from '../config/mongoose';

const {
  User
} = mongoose;

export default (app) => {
  const jwtOptions = {
    jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.JWT_SECRET,
  };

  const strategy = new passportJWT.Strategy(jwtOptions, (jwtPayload, next) => {
    const {
      id
    } = jwtPayload;

    // here put code, which invalidate token if was logged out from client side
    // dont need at time ..

    User.find({ id, active: true })
      .then((user) => {
        if (user) {
          return next(null, merge(user, jwtPayload));
        }

        return next(null, false);
      });
  });

  passport.use(strategy);
  app.use(passport.initialize());
};

