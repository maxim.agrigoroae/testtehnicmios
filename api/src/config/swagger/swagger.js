import fs from 'fs';
import path from 'path';

import swaggerUi from 'swagger-ui-express';
import swaggerDoc from './swagger.json';
import pathsDoc from './paths.json';
import definitionsDoc from './definitions.json';
import config from '../environment';
import Log from '../../utilities/log';
import packageJson from '../../../package.json';

const { version, name, description } = packageJson;

export default (app) => {
  const apiDir = path.join(config.root, 'api');
  const files = fs.readdirSync(apiDir);

  files.forEach((file) => {
    const apiModule = path.join(apiDir, file);
    if (fs.statSync(apiModule).isDirectory()) {
      if (fs.existsSync(path.join(apiModule, 'paths.swagger.json'))) {
        Log.info(`Loading API module swagger paths :: ${file}`);
        const apiModulePaths = JSON.parse(fs.readFileSync(path.join(apiModule, 'paths.swagger.json')));

        Object.assign(pathsDoc, apiModulePaths);
      }

      if (fs.existsSync(path.join(apiModule, 'definitions.swagger.json'))) {
        Log.info(`Loading API module swagger definitions :: ${file}`);
        const apiModuleDefinitions = JSON.parse(fs.readFileSync(path.join(apiModule, 'definitions.swagger.json')));

        Object.assign(definitionsDoc, apiModuleDefinitions);
      }
    }
  });

  swaggerDoc.paths = pathsDoc;
  swaggerDoc.definitions = definitionsDoc;
  swaggerDoc.host = config.swagger.host;
  swaggerDoc.info.version = version;
  swaggerDoc.info.title = name;
  swaggerDoc.info.description = description;

  app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));
};
