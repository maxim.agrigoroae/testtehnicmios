import express from 'express';
import http from 'http';

import config from './config/environment';
import configureExpress from './config/express';
import socket from './config/socket';
import configurePassport from './config/passport';
import configureSwagger from './config/swagger/swagger';
import mongoose from './config/mongoose';

import setupRoutes from './api/routes';
import Log from './utilities/log';

// Setup server
const app = express();
const server = http.createServer(app);

configureExpress(app);

socket.io = socket.configureSocket(server);

configurePassport(app);

setupRoutes(app);

configureSwagger(app);

mongoose.connect()
  .catch(err => Log.error(`${err.name}: ${err.message}`))
  .finally(() => {
    server.listen(config.port, config.ip, () => {
      Log.info(`Express server listening on ${config.port}, in ${app.get('env')} mode`);
    });
  });

export default app;
